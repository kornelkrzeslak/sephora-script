1. Fill `desktop.json` and `mobile.json` based on `example.json`.
2. Create `.env` file based on `.env.example`.
3. Run `yarn build`.
4. Run `yarn desktop` to insert values to DB from `desktop.json`.
5. Run `yarn mobile` to insert values to DB from `mobile.json`.
