export const checkFileBody = (category: any) => {
  if (!category.name) throw new Error(`Category name is required.`);
  if (!category.sections) throw new Error(`Sections are required.`);

  const { sections } = category;

  for (let i = 0; i < sections.length; i++) {
    if (!sections[i].name) throw new Error(`Section name is required. Missing at index ${i}.`);
    if (!sections[i].components) throw new Error(`Components are required. Missing in section: '${sections[i].name}' at index ${i}.`);

    const { components } = sections[i];

    for (let i = 0; i < components.length; i++) {
      if (!components[i].name) throw new Error(`Component name is required. Missing at index ${i} in section '${sections[i].name}'.`);
      if (!components[i].type) throw new Error(`Component type is required. Missing in component: '${components[i].name}' at index ${i} in section '${sections[i].name}'.`);
    }
  }
}