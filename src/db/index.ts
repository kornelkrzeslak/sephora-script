const { Pool } = require('pg')

const {
  PG_HOST: host,
  PG_PORT: port,
  PG_USER: user,
  PG_PASSWORD: password,
  PG_DATABASE: database,
} = process.env;

export const pool = new Pool({
  host,
  database,
  user,
  password,
  port: parseInt(port as string)
});

export const query = (text: string, params: any) => pool.query(text, params);

export const endPool = async () => await pool.end(() => console.log('DB connection closed. Pool has ended successfully.'));

export const insertCategorySectionsComponents = async (category: any) => {
  const { name: categoryName, sections } = category;

  const client = await pool.connect()
  try {
    console.log('Beginning transaction.');
    await client.query('BEGIN');

    // console.log(`Category: ${categoryName}`);

    const { rows: countRows } = await client.query(
      'SELECT COUNT(*) FROM categories WHERE name = $1',
      [categoryName]
    );

    if (countRows[0].count > 0) {
      throw new Error(`Category '${categoryName}' already exists`);
    }

    const isActive = true;

    const { rows } = await client.query(
      'INSERT INTO categories(name, "isActive") VALUES($1, $2) RETURNING *',
      [categoryName, isActive]
    );

    const { id: categoryId } = rows[0];
    // console.log(rows, categoryId)

    for (let i = 0; i < sections.length; i++) {
      const { name: sectionName, components } = sections[i];

      const { rows: countRows } = await client.query(
        'SELECT COUNT(*) FROM sections WHERE name = $1',
        [sectionName]
      );

      if (countRows[0].count > 0) {
        throw new Error(`Section '${sectionName}' already exists`);
      }
      // console.log(category.type);

      const categoryType = 'category' + category.type;
      // console.log(categoryType);

      // console.log(`\tSection: ${sectionName} #${i}`);

      const { rows } = await client.query(
        `INSERT INTO sections(name, "order", "${categoryType}") VALUES($1, $2, $3) RETURNING *`,
        [sectionName, i, categoryId]
      );
      const { id: sectionId } = rows[0];
      // console.log(rows, sectionId)

      for (let i = 0; i < components.length; i++) {
        const { type: componentType, name: componentName } = components[i];

        const { rows: countRows } = await client.query(
          'SELECT COUNT(*) FROM components WHERE name = $1',
          [componentName]
        );

        if (countRows[0].count > 0) {
          throw new Error(`Component '${componentName}' already exists`);
        }

        // console.log(`\t\tComponentType: ${componentType} ComponentName: ${componentName}`)

        const { rows } = await client.query(
          'INSERT INTO components(name, type) VALUES($1, $2) RETURNING *',
          [componentName, componentType]
        );
        const { id: componentId } = rows[0];
        // console.log(rows, componentId)

        const { rows: rows2 } = await client.query(
          `INSERT INTO components_sections__sections_components(section_id, component_id) VALUES($1, $2) RETURNING *`,
          [sectionId, componentId]
        );
        // console.log(rows2)
      }

      await client.query('COMMIT')
      console.log('Successfully commited all queries.')
    }
  } catch ({ message }) {
    console.log(message, '- rolling back transaction...');

    await client.query('ROLLBACK');
  } finally {
    client.release();

    await endPool();
  }
}