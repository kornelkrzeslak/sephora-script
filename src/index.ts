import fs from 'fs';
import yargs = require('yargs/yargs');

require('dotenv').config();

import { insertCategorySectionsComponents } from './db';
import { checkFileBody } from './utils'

const argv = yargs(process.argv.slice(2)).options({
  f: {
    alias: 'file',
    demandOption: 'File is required.',
    describe: 'Creates database objects based on given JSON file.',
    type: 'string',
    nargs: 1,
  },
}).argv;

const filename = argv.f;

// Correct files
const EXAMPLE_FILE = 'example.json';

// Files with missing properties
const ERR_CATEGORY_NAME = 'files/err-category-name.json';
const ERR_CATEGORY_SECTIONS = 'files/err-category-sections.json';
const ERR_COMPONENT_NAME = 'files/err-component-name.json';
const ERR_COMPONENT_TYPE = 'files/err-component-type.json';
const ERR_SECTION_COMPONENTS = 'files/err-section-components.json';
const ERR_SECTION_NAME = 'files/err-section-name.json';

(async () => {
  try {
    const category = JSON.parse(fs.readFileSync(filename, 'utf8'));

    checkFileBody(category);

    await insertCategorySectionsComponents(category);
  } catch ({ message }) {
    console.log(message);
  }
})();